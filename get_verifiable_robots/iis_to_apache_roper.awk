# This awk script takes a Microsoft IIS Web server log file in the default format
# and converts it into NCSA common format. If you don't have admin access to your web server
# so that you can run Microsoft's convlog.exe, you may need this script. I do!
#
# This was written and tested under the free GNU awk 3.0.4 compiled for Win32
#
# If you are new to awk, do the following on a Win32 machine:
# 1) Save this file (without the HTML codes) as "convlog.awk" in the directory where your IIS-format log
#    file is. Or you might be better off just cutting and pasting from this Web page to a blank text file
#    instead of saving this page directly from the Web.
# 2) Modify the "-0500" part of the last line of code below to match the GMT offset in hours for your
#    server.
# 3) If you don't have awk, get and install GNU awk from the Web (e.g., from www.gnu.org ).
# 4) Open a DOS window and go to the directory where you saved this script and your IIS log file called logfilename.log
# 5) Type in: awk -f convlog.awk logfilename.log > whatever.log
#
# Now you can do whatever you want to do with the whatever.log file, which will be in NCSA common format.
# For example, I use Darryl Burgdorfer's free Perl script called "Weblog" from http://awsd.com/scripts/weblog/index.shtml
#
# Microsoft summarizes the log file formats involved here at
# http://www.microsoft.com/windows2000/en/professional/iis/default.asp?url=/WINDOWS2000/en/professional/iis/htm/core/iiabtlg.htm
#
# Apache describes NCSA common format here: http://httpd.apache.org/docs/mod/mod_log_common.html
#
# This script will need a minor fix in the year 2100 and then at each consecutive century :)
# Copyright (C) February 2001 Alan Ng nospamalan@alan-ng.net. Remove "nospam" to contact me.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# You can view a copy of the GNU General Public License at:
# http://www.gnu.org/copyleft/gpl.html

BEGIN {
months[1] = "Jan"
months[2] = "Feb"
months[3] = "Mar"
months[4] = "Apr"
months[5] = "May"
months[6] = "Jun"
months[7] = "Jul"
months[8] = "Aug"
months[9] = "Sep"
months[10] = "Oct"
months[11] = "Nov"
months[12] = "Dec"
}

{								# MAIN
					
gsub( /,/ , "")							#delete comma delimiters

split ($1, date, "-")					#parse date
date[3]=sprintf ("%02d" , date[3] )		#make the day two-digit with leading zero 2 for huskyLogs, 3 for roper
date[2]=months[date[2]]					#month 3 for huskyLogs, 2 for roper
split ($2, time, ":")					#parse time
time[1]=sprintf ("%02d" , time[1] )		#make the hour two-digit with leading zero
										#output modified 0$
print $11 " " $16 " - - [" date[3] "/" date[2] "/" date[1] ":" $2 " -0500] \"" $6 " " $7 " " $12 "\" " $17 " " $20 " \"-\" \"" $13 "\""
}								# END MAIN