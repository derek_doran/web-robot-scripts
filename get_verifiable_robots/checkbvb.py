from bs4 import BeautifulSoup
from urllib2 import urlopen
from urllib2 import HTTPError

BASE_URL = "http://www.botsvsbrowsers.com/listings.asp?search="
IP_URL = "http://www.botsvsbrowsers.com/ip/"

## Returns:
## 0 - Unknown 
## 1 -  a robot
## 2 -  a human
def query_bvb_database(query):
	try:
		html = urlopen(BASE_URL+query).read()
		soup = BeautifulSoup(html,"lxml")
		table = soup.find("table","UATable")
		if table is not None:
			img_tags = [str(x) for x in table.findAll("img")]
			labels =  [ x[x.find("alt")+5:x.find("alt")+8] for x in img_tags]
			if(len(labels) > 0):
				rob_labels = [i for i,x in enumerate(labels) if x == "Bot"]
				percentage = float(len(rob_labels)/len(labels))
				if percentage > 0.75:
					return 1
				elif percentage < 0.25:
					return 2
		return 0
	except HTTPError:
		return 0

##Returns: 
## 1 - yes
## 0 - unknown
def is_human_ip_address(ip):
	try:	
		html = urlopen(IP_URL+ip).read()
		soup = BeautifulSoup(html,"lxml")
		table = soup.find("table","UATable")
		if table is not None:
			img_tags = [str(x) for x in table.findAll("img")]
			labels =  [ x[x.find("alt")+5:x.find("alt")+8] for x in img_tags]
			if(len(labels) > 0):
				browser_labels = [i for i,x in enumerate(labels) if x == "Bro"]
				if len(browser_labels) == len(labels):
					return 1
		return 0
	except HTTPError:
		return 0

