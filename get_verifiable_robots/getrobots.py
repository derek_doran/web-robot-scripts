from bs4 import BeautifulSoup
from urllib2 import urlopen
from urllib2 import HTTPError
import apachelog
import sys
import re
import fileinput
import time

BASE_URL = "http://www.botsvsbrowsers.com/listings.asp?search="
IP_URL = "http://www.botsvsbrowsers.com/ip/"

## Returns:
## 0 - Unknown 
## 1 -  a robot
## 2 -  a human
def query_bvb_database(query):
    try:
        html = urlopen(BASE_URL+query).read()
        soup = BeautifulSoup(html,"lxml")
        table = soup.find("table","UATable")
        if table is not None:
            img_tags = [str(x) for x in table.findAll("img")]
            labels =  [ x[x.find("alt")+5:x.find("alt")+8] for x in img_tags]
            if(len(labels) > 0):
                rob_labels = [i for i,x in enumerate(labels) if x == "Bot"]
                percentage = float(len(rob_labels)/len(labels))
                if percentage > 0.75:
                    return 1
                elif percentage < 0.25:
                    return 2
        return 0
    except HTTPError:
        return 0

##Returns: 
## 1 - yes
## 0 - unknown
def is_human_ip_address(ip):
    try:    
        html = urlopen(IP_URL+ip).read()
        soup = BeautifulSoup(html,"lxml")
        table = soup.find("table","UATable")
        if table is not None:
            img_tags = [str(x) for x in table.findAll("img")]
            labels =  [ x[x.find("alt")+5:x.find("alt")+8] for x in img_tags]
            if(len(labels) > 0):
                browser_labels = [i for i,x in enumerate(labels) if x == "Bro"]
                if len(browser_labels) == len(labels):
                    return 1
        return 0
    except HTTPError:
        return 0


start = time.time()
p = apachelog.parser(r'%h %v %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"')

if len(sys.argv) > 1:
	logname = sys.argv[1]
else:
	logname = "sumo2Sample.log"

robotLog = open(logname[:-4]+'.rob',"w")
humanLog = open(logname[:-4]+'.hum',"w")
unkLog = open(logname[:-4]+'.unk',"w")

#First load our database of robot regEx's.
knownRobots = []
for bot in fileinput.input("robots-db1.pm"):
	aBot = re.split('\n',bot)
	theBot = aBot[0].strip('\'')
	#also take out any carriage returns, in case
	theBot = theBot.replace("\r", "")
	#also, if the last character is a single quote, remove it.
	if len(theBot) > 0:
		if theBot[-1] == "\'":    
			theBot = theBot[0:-1]
		knownRobots.append(theBot)

## Keep a record of past seen user-agent and ip address combos.. no need to requery if we know their type.
seen = {}
k=0
miss=0
done=0
for line in open(logname):
    k+=1
    if k % 1000 == 0:
        print str(k)+' /'+str(sys.argv[2])
        print 'Elapsed time: '+str(time.time()-start)
    bvb_code = 0
    try:
        agent = p.parse(line)['%{User-agent}i']
        ip = p.parse(line)['%h']
        key=ip+agent
        if key in seen:
            if seen[key] == 1:
                robotLog.write(line)
            if seen[key] == 2:
                humanLog.write(line)
            if seen[key] == 0:
                unkLog.write(line)
        else: 
            for robot in knownRobots:
            	if re.search(robot,agent, re.I):
            		bvb_code = 1
            		break

            if bvb_code == 0:
            	bvb_code = query_bvb_database(agent)

            if bvb_code == 1:
            	robotLog.write(line)
                seen[key] = 1

            elif ip.startswith('137.99'):
                #print 'human hit!'
                humanLog.write(line)
                seen[key] = 2

            ## just not sure if python how short circuits in ifs work in python
            ## if the bvb query only returns human user-agents, double check that the ip address 
            ## has confirmed human user-agent's associated with it 
            elif is_human_ip_address(ip) == 1:
                #print 'human hit!'
            	humanLog.write(line)
                seen[key] = 2
            else:
                #print 'unk hit!'
            	unkLog.write(line)
                seen[key] = 0
        done=done+1
    except:
        miss=miss+1
        done=done+1
        sys.stderr.write("Unable to parse %s / %s" % (str(miss), str(done)))
       
robotLog.close()
humanLog.close()
unkLog.close()
print time.time() - start

###
## Sample code to implement SMOTE in R using DMwR package
## require(DMwR)
## ncols<-ncol(dm)
## dm<-cbind(dm[2:ncols],dm[1])
## dmSmote<-SMOTE(target ~ . , dm,k=5,perc.over = 1400,perc.under=140)
## dm<-cbind(dmSmote[ncols],dmSmote[1:ncols-1])
## Here dm is the data frame of features and target is the variable I want to 
## predict.Please note that it is compulsory that the dependent variable that one 
## needs to predict (column "target" in my case) needs to be at the end of the data frame. 
## I missed that initially and spent quite a few minutes figuring out the problem.